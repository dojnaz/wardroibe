package com.wardroibe

class WardroibeItem {
    var gender: Boolean? = null

    var isHead: Boolean? = null
    var isBody: Boolean? = null
    var isLeg: Boolean? = null
    var isFeet: Boolean? = null

    var imagePath: String? = null
}
