package com.wardroibe.ui

class WardroibeListItem {
    private var image: String? = null
    private var type: String? = null

    fun getImage(): String? {
        return image
    }
    fun setImage(image: String) {
        this.image = image
    }
    fun getType(): String? {
        return type
    }
    fun setType(image: String) {
        this.type = type
    }
}