package com.wardroibe.ui.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.wardroibe.MainActivity
import com.wardroibe.R
import com.wardroibe.WardroibeItem
import com.wardroibe.ui.WardroibeItemAdapter
import java.io.File

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_list, container, false)

        var itemList = ArrayList<WardroibeItem>()
        File(MainActivity.context!!.filesDir.absolutePath + "/WardroideItems/").walk().forEach {
            Log.i("WarLoad", it.absolutePath)
            if (!it.isDirectory) {
                val wardroibeItem =
                    Gson().fromJson(it.absoluteFile.readText(), WardroibeItem::class.java)
                itemList.add(wardroibeItem)
            }
        }
        val recyclerView = root.findViewById<RecyclerView>(R.id.recycler_view)
        var iAdapter = WardroibeItemAdapter(itemList)
        var iLayoutManager = LinearLayoutManager(MainActivity.context!!)
        recyclerView.layoutManager = iLayoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.addItemDecoration(DividerItemDecoration(MainActivity.context, LinearLayoutManager.VERTICAL))
        recyclerView.adapter = iAdapter

        iAdapter.notifyDataSetChanged()

        return root
    }
}
