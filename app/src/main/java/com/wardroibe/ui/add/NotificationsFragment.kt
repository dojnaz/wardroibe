package com.wardroibe.ui.add

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.ComponentActivity
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.wardroibe.MainActivity
import com.wardroibe.R
import com.wardroibe.WardroibeItem
import java.io.File
import java.io.FileOutputStream
import java.net.URI
import java.util.*


class NotificationsFragment : Fragment() {
    private lateinit var addViewModel: AddViewModel
    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_GALLERY = 2
    var capturebutton: ImageButton? = null;
    var gender: Int? = null;

    var lastBitmap: Bitmap? = null;

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        addViewModel =
            ViewModelProviders.of(this).get(AddViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_add, container, false)

        val genderswitch: Switch = root.findViewById(R.id.GenderSwitch)
        val Headswitch: Switch = root.findViewById(R.id.HeadSwitch)
        val Bodyswitch: Switch = root.findViewById(R.id.BodySwitch)
        val Legsswitch: Switch = root.findViewById(R.id.LegsSwitch)
        val Feetswitch: Switch = root.findViewById(R.id.FeetSwitch)
        val add: Button = root.findViewById(R.id.Add)
        val Body: ImageView = root.findViewById(R.id.Doll)

        add.setOnClickListener {
            var wardroibeItem = WardroibeItem()
            wardroibeItem.gender = genderswitch.isChecked
            wardroibeItem.isHead = Headswitch.isChecked
            wardroibeItem.isBody = Bodyswitch.isChecked
            wardroibeItem.isLeg = Legsswitch.isChecked
            wardroibeItem.isFeet = Feetswitch.isChecked
            if (!(wardroibeItem.isHead!! || wardroibeItem.isBody!! || wardroibeItem.isLeg!! || wardroibeItem.isFeet!!)) {
                Toast.makeText(
                    MainActivity.context,
                    "You need to choose a body part!",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            if (lastBitmap == null) {
                Toast.makeText(
                    MainActivity.context,
                    "You need to take a picture!",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            val itemDir = File(MainActivity.context!!.filesDir.absolutePath + "/WardroideItems")
            if (!itemDir.exists()) {
                itemDir.mkdir()
            }
            var uuidItem: UUID;
            while (true) {
                var collision = false
                uuidItem = UUID.randomUUID()
                itemDir.walk().forEach {
                    if (it.isDirectory) {
                        Log.i("AddBtn", it.nameWithoutExtension)
                        if (uuidItem.toString() == it.nameWithoutExtension) {
                            collision = true
                        }
                    }
                }
                if (!collision) break
            }

            val imgDir = File(MainActivity.context!!.filesDir.absolutePath + "/WardroideImages")
            if (!imgDir.exists()) {
                imgDir.mkdir()
            }
            var uuidImg: UUID;
            while (true) {
                var collision = false
                uuidImg = UUID.randomUUID()
                imgDir.walk().forEach {
                    if (it.isDirectory) {
                        Log.i("AddBtn", it.nameWithoutExtension)
                        if (uuidImg.toString() == it.nameWithoutExtension) {
                            collision = true
                        }
                    }
                }
                if (!collision) break
            }
            val img = File(MainActivity.context!!.filesDir.absolutePath + "/WardroideImages/" + uuidImg.toString() + ".webp")
            val out = FileOutputStream(img)

            wardroibeItem.imagePath = img.absolutePath

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                lastBitmap!!.compress(Bitmap.CompressFormat.WEBP_LOSSLESS, 85, out)
            }
            else {
                lastBitmap!!.compress(Bitmap.CompressFormat.JPEG, 85, out)
            }
            out.flush()
            out.close()

            File(MainActivity.context!!.filesDir.absolutePath + "/WardroideItems/" + uuidItem.toString() + ".json").writeText(Gson().toJson(wardroibeItem))
        }

        genderswitch.setOnClickListener {
            if (genderswitch.isChecked) {
                genderswitch.text = "Female"; gender = 1; Body.setImageResource(R.drawable.female)
            } else {
                genderswitch.text = "Male"; gender = 0; Body.setImageResource(R.drawable.male)
            }
        }


        Headswitch.setOnClickListener {
            if (Headswitch.isChecked) {
                Bodyswitch.setChecked(false); Legsswitch.setChecked(false); Feetswitch.setChecked(
                    false
                ); if (gender == 0) {
                    Body.setImageResource(R.drawable.male_head)
                } else (Body.setImageResource(R.drawable.female_head))
            }
        }
        Bodyswitch.setOnClickListener {
            if (Bodyswitch.isChecked) {
                Headswitch.setChecked(false); Legsswitch.setChecked(false); Feetswitch.setChecked(
                    false
                ); if (gender == 0) {
                    Body.setImageResource(R.drawable.male_body)
                } else (Body.setImageResource(R.drawable.female_body))
            }
        }
        Legsswitch.setOnClickListener {
            if (Legsswitch.isChecked) {
                Headswitch.setChecked(false); Bodyswitch.setChecked(false); Feetswitch.setChecked(
                    false
                );if (gender == 0) {
                    Body.setImageResource(R.drawable.male_legs)
                } else (Body.setImageResource(R.drawable.female_legs))
            }
        }
        Feetswitch.setOnClickListener {
            if (Feetswitch.isChecked) {
                Headswitch.setChecked(false); Bodyswitch.setChecked(false); Legsswitch.setChecked(
                    false
                ); if (gender == 0) {
                    Body.setImageResource(R.drawable.male_feet)
                } else (Body.setImageResource(R.drawable.female_feet))
            }
        }


        capturebutton = root.findViewById(R.id.capture_btn)
        capturebutton!!.setOnClickListener {
            when {
                ContextCompat.checkSelfPermission(
                    MainActivity.context!!,
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED -> {
                    // You can use the API that requires the permission.
                    // In an educational UI, explain to the user why your app requires this
                    // permission for a specific feature to behave as expected. In this UI,
                    // include a "cancel" or "no thanks" button that allows the user to
                    // continue using your app without granting the permission.
                    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    try {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                    } catch (e: ActivityNotFoundException) {
                        // display error state to the user
                    }
                }
                else -> {
                    // You can directly ask for the permission.
                    // The registered ActivityResultCallback gets the result of this request.
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), 0)
                }
            }
        }
        return root;
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == AppCompatActivity.RESULT_OK) {
            val imageBitmap = data!!.extras!!.get("data") as Bitmap
            lastBitmap = imageBitmap
            capturebutton?.setImageBitmap(imageBitmap)
        }

        if (requestCode == REQUEST_GALLERY && resultCode == AppCompatActivity.RESULT_OK) {
            lastBitmap = BitmapFactory.decodeFile(data?.data!!.path!!)
            capturebutton?.setImageURI(data.data)
        }
    }
}
