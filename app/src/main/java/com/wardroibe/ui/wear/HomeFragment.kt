package com.wardroibe.ui.wear

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Switch
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.wardroibe.MainActivity
import com.wardroibe.R
import com.wardroibe.WardroibeItem
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_add.*
import java.io.File
import kotlin.random.Random

class HomeFragment : Fragment() {
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_wear, container, false)
        val RndBtn: Button = root.findViewById(R.id.Randomize)

        RndBtn.setOnClickListener {
            val HeadImage: ImageView = root.findViewById(R.id.HeadImage)
            val BodyImage: ImageView = root.findViewById(R.id.BodyImage)
            val LegsImage: ImageView = root.findViewById(R.id.LegsImage)
            val FeetImage: ImageView = root.findViewById(R.id.FeetImage)

            val headList = ArrayList<WardroibeItem>()
            val bodyList = ArrayList<WardroibeItem>()
            val legsList = ArrayList<WardroibeItem>()
            val feetList = ArrayList<WardroibeItem>()

            File(MainActivity.context!!.filesDir.absolutePath + "/WardroideItems/").walk().forEach {
                Log.i("WarLoad", it.absolutePath)
                if (!it.isDirectory) {
                    val wardroibeItem =
                        Gson().fromJson(it.absoluteFile.readText(), WardroibeItem::class.java)
                    if (wardroibeItem.isHead!!) headList.add(wardroibeItem)
                    if (wardroibeItem.isBody!!) bodyList.add(wardroibeItem)
                    if (wardroibeItem.isLeg!!) legsList.add(wardroibeItem)
                    if (wardroibeItem.isFeet!!) feetList.add(wardroibeItem)
                }
            }

            if (headList.size > 0) {
                val headRnd = Random(System.nanoTime()).nextInt(0, headList.size)
                HeadImage.setImageURI(Uri.fromFile(File(headList[headRnd].imagePath!!)))
            }
            if (bodyList.size > 0) {
                val bodyRnd = Random(System.nanoTime()).nextInt(0, bodyList.size)
                BodyImage.setImageURI(Uri.fromFile(File(bodyList[bodyRnd].imagePath!!)))
            }
            if (legsList.size > 0) {
                val legsRnd = Random(System.nanoTime()).nextInt(0, legsList.size)
                LegsImage.setImageURI(Uri.fromFile(File(legsList[legsRnd].imagePath!!)))
            }
            if (feetList.size > 0) {
                val feetRnd = Random(System.nanoTime()).nextInt(0, feetList.size)
                FeetImage.setImageURI(Uri.fromFile(File(feetList[feetRnd].imagePath!!)))
            }
        }
        RndBtn.performClick()

        return root
    }
}