package com.wardroibe.ui

import android.media.Image
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.wardroibe.R
import com.wardroibe.WardroibeItem
import kotlinx.android.synthetic.main.wardroibe_list_item.view.*
import java.io.File
import java.net.URI

class WardroibeItemAdapter(list: List<WardroibeItem>) :
    RecyclerView.Adapter<WardroibeItemAdapter.MyViewHolder>() {
    var itemList: List<WardroibeItem>? = list

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var image: ImageView? = null;
        var type: TextView? = null;

        init {
            image = view.findViewById(R.id.ItemPic)
            type = view.findViewById(R.id.ItemName)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.wardroibe_list_item, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = itemList!!.get(position)
        Log.i("WarBind", Gson().toJson(item))
        holder.image!!.setImageURI(Uri.fromFile(File(item.imagePath!!)))
        if (item.isHead!!) { holder.type!!.text = "Head" }
        if (item.isBody!!) { holder.type!!.text = "Body" }
        if (item.isLeg!!) { holder.type!!.text = "Legs" }
        if (item.isFeet!!) { holder.type!!.text = "Feet" }
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }
}